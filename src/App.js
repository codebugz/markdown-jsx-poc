import React from "react";
import logo from "./logo.svg";
import Markdown from "markdown-to-jsx";
import "./App.css";

const MyComponent = ({ children }) => {
  const handleClick = () => {
    alert("Button clicked!");
  };
  return (
    <>
      <h5>{children}</h5>
      <button onClick={handleClick}>Click me</button>
    </>
  );
};
function App() {
  const [content, setContent] = React.useState("");

  React.useEffect(() => {
    fetch(`/data/sample.md`)
      .then((res) => res.text())
      .then((response) => setContent(response))
      .catch((err) => setContent(err));
  }, []);

  return (
    <div className="App">
      <Markdown
        options={{
          overrides: {
            MyComponent: {
              component: MyComponent,
            },
          },
        }}>
        {content}
      </Markdown>
    </div>
  );
}

export default App;
